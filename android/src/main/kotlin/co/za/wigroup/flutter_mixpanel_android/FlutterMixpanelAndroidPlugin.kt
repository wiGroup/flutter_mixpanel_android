package co.za.wigroup.flutter_mixpanel_android

import android.app.Activity
import android.content.Context
import androidx.annotation.NonNull;
import com.mixpanel.android.mpmetrics.MixpanelAPI

import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.embedding.engine.plugins.activity.ActivityAware
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import io.flutter.plugin.common.PluginRegistry.Registrar
import org.json.JSONObject

/** FlutterMixpanelAndroidPlugin */
public class FlutterMixpanelAndroidPlugin: FlutterPlugin, MethodCallHandler, ActivityAware {

  /// The MethodChannel that will the communication between Flutter and native Android
  ///
  /// This local reference serves to register the plugin with the Flutter Engine and unregister it
  /// when the Flutter Engine is detached from the Activity
  private lateinit var channel : MethodChannel
  private var mixpanel: MixpanelAPI? = null

  private fun jsonToNumberMap(jsonObj: JSONObject): Map<String, Double> {
    var map = HashMap<String, Double>();
    var keys: Iterator<String> = jsonObj.keys();
    while (keys.hasNext()) {
      var key = keys.next();
      map.put(key, jsonObj.getDouble(key));
    }
    return map;
  }

  override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
    channel = MethodChannel(flutterPluginBinding.getFlutterEngine().getDartExecutor(), "flutter_mixpanel_android")
    channel.setMethodCallHandler(this);
  }

  // This static function is optional and equivalent to onAttachedToEngine. It supports the old
  // pre-Flutter-1.12 Android projects. You are encouraged to continue supporting
  // plugin registration via this function while apps migrate to use the new Android APIs
  // post-flutter-1.12 via https://flutter.dev/go/android-project-migration.
  //
  // It is encouraged to share logic between onAttachedToEngine and registerWith to keep
  // them functionally equivalent. Only one of onAttachedToEngine or registerWith will be called
  // depending on the user's project. onAttachedToEngine or registerWith must both be defined
  // in the same class.
  companion object {
    var ctxt: Context? = null
    var activity: Activity? = null

    @JvmStatic
    fun registerWith(registrar: Registrar) {
      ctxt = registrar.context()
      val channel = MethodChannel(registrar.messenger(), "flutter_mixpanel_android")
      channel.setMethodCallHandler(FlutterMixpanelAndroidPlugin())

      if(this.activity == null){
        this.activity = registrar.activity();
      }
    }
  }

  override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: Result) {
    if (call.method == "initialize") {
      mixpanel = MixpanelAPI.getInstance(ctxt, call.arguments.toString())
      result.success("Init success..")

    } else if (call.method == "identify") {
      mixpanel?.identify(call.arguments.toString())
      result.success("Identify success..")
    } else if (call.method == "identifyPeople") {
      val id = call.arguments.toString()
      mixpanel?.identify(id)
      mixpanel?.people?.identify(id)
      result.success("Identify people success..")
    } else if (call.method == "alias") {
      mixpanel?.alias(call.arguments.toString(), mixpanel?.getDistinctId())
      result.success("Alias success..")

    } else if (call.method == "setPeopleProperties") {
      if (call.arguments == null) {
        result.error("Parse Error", "Arguments required for setPeopleProperties platform call", null)
      } else {
        val json = JSONObject(call.arguments.toString())
        mixpanel?.people?.set(json)
        result.success("Set People Properties success..")
      }
    } else if (call.method == "setOnce") {
      if (call.arguments == null) {
        result.error("Parse Error", "Arguments required for setOnnce platform call", null)
      } else {
        val json = JSONObject(call.arguments.toString())
        mixpanel?.people?.setOnce(json)
        result.success("Set Once success..")
      }
    } else if (call.method == "incrementPeopleProperties") {
      if (call.arguments == null) {
        result.error("Parse Error", "Arguments required for incrementPeopleProperties platform call", null)
      } else {
        val map = jsonToNumberMap(JSONObject(call.arguments.toString()))
        mixpanel?.people?.increment(map)
        result.success("Increment People Properties success..")
      }
    } else if (call.method == "registerSuperProperties") {
      if (call.arguments == null) {
        result.error("Parse Error", "Arguments required for registerSuperProperties platform call", null)
      } else {
        val json = JSONObject(call.arguments.toString())
        mixpanel?.registerSuperProperties(json)
        result.success("Register Properties success..")
      }
    } else if (call.method == "reset") {
      mixpanel?.reset()
      result.success("Reset success..")
    } else if (call.method == "flush") {
      mixpanel?.flush()
      result.success("Flush success..")
    } else if (call.method == "showNotificationIfAvailable") {
      mixpanel?.people?.showNotificationIfAvailable(FlutterMixpanelAndroidPlugin.activity)
    } else if (call.method == "removeAllPushDeviceTokens") {
      mixpanel?.people?.clearPushRegistrationId()
    } else if (call.method == "addPushDeviceToken") {
      mixpanel?.people?.pushRegistrationId = call.arguments.toString()
    } else {
      if(call.arguments == null) {
        mixpanel?.track(call.method)
      } else {
        val json = JSONObject(call.arguments.toString())
        mixpanel?.track(call.method, json)
      }
      result.success("Track success..")
    }
  }

  override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
    channel.setMethodCallHandler(null)
  }

  override fun onAttachedToActivity(binding: ActivityPluginBinding) {
    if (FlutterMixpanelAndroidPlugin.activity == null) {
      FlutterMixpanelAndroidPlugin.activity = binding.activity
    }
  }

  override fun onDetachedFromActivityForConfigChanges() {
    TODO("Not yet implemented")
  }

  override fun onReattachedToActivityForConfigChanges(binding: ActivityPluginBinding) {
    TODO("Not yet implemented")
  }

  override fun onDetachedFromActivity() {
    TODO("Not yet implemented")
  }
}
