#import "FlutterMixpanelAndroidPlugin.h"
#if __has_include(<flutter_mixpanel_android/flutter_mixpanel_android-Swift.h>)
#import <flutter_mixpanel_android/flutter_mixpanel_android-Swift.h>
#else
// Support project import fallback if the generated compatibility header
// is not copied when this plugin is created as a library.
// https://forums.swift.org/t/swift-static-libraries-dont-copy-generated-objective-c-header/19816
#import "flutter_mixpanel_android-Swift.h"
#endif

@implementation FlutterMixpanelAndroidPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftFlutterMixpanelAndroidPlugin registerWithRegistrar:registrar];
}
@end
