import 'dart:async';
import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

abstract class _FlutterMixpanelAndroid {
  Future track(String eventName, [dynamic props]);
}

class _FlutterMixpanelAndroidOptedOut extends _FlutterMixpanelAndroid {
  Future track(String eventName, [dynamic props]) {
    return Future.value();
  }
}

class _FlutterMixpanelAndroidOptedIn extends _FlutterMixpanelAndroid {
  final MethodChannel _channel = MethodChannel('flutter_mixpanel_android');

  @override
  Future track(String eventName, [props]) async {
    return await _channel.invokeMethod(eventName, props);
  }
}

class _FlutterMixpanelAndroidDebug extends _FlutterMixpanelAndroid {
  final _FlutterMixpanelAndroid child;
  _FlutterMixpanelAndroidDebug({this.child});

  Future track(String eventName, [dynamic props]) async {
    String debugMessage = """
    Event: $eventName with properties: $props
    """;
    debugPrint(debugMessage);

    return await this.child.track(eventName, props);
  }
}

class FlutterMixpanelAndroid extends _FlutterMixpanelAndroid {
  bool enableEventLogging = true;
  bool isOptOut = false;

  _FlutterMixpanelAndroid _fmp;

  FlutterMixpanelAndroid({this.enableEventLogging, this.isOptOut}) {
    _FlutterMixpanelAndroid _flutterMixpanel = isOptOut
        ? _FlutterMixpanelAndroidOptedOut()
        : _FlutterMixpanelAndroidOptedIn();
    (enableEventLogging)
        ? _fmp = _FlutterMixpanelAndroidDebug(child: _flutterMixpanel)
        : _fmp = _flutterMixpanel;
  }

  Future initialize(String token) async {
    return this._fmp.track('initialize', token);
  }

  Future identify(String distinctId) async {
    return this._fmp.track('identify', distinctId);
  }

  Future identifyPeople(String distinctId) async {
    await this._fmp.track('identifyPeople', distinctId);
  }

  Future showNotificationIfAvailable() async {
    await this._fmp.track('showNotificationIfAvailable');
  }

  Future setPeopleProperties([props]) async {
    await this._fmp.track('setPeopleProperties', jsonEncode(props));
  }

  Future addPushIosDeviceToken(String token) async {
    await this._fmp.track('addPushIosDeviceToken', token);
  }

  Future removeAllPushDeviceTokens() async {
    await this._fmp.track('removeAllPushDeviceTokens');
  }

  Future setOnce([props]) async {
    await this._fmp.track('setOnce', jsonEncode(props));
  }

  Future incrementPeopleProperties(Map<String, double> props) {
    return this._fmp.track('incrementPeopleProperties', jsonEncode(props));
  }

  Future setUserId(String distinctId) async {
    return this._fmp.track('setUserId', distinctId);
  }

  Future alias(String alias) async {
    return this._fmp.track('alias', alias);
  }

  Future flush() async {
    return this._fmp.track('flush');
  }

  Future reset() async {
    return this._fmp.track('reset');
  }

  Future track(String eventName, [props]) {
    return this._fmp.track(eventName, jsonEncode(props));
  }
}
